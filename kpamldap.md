

kserver

docker run --rm --name kserver.edt.org -h kserver.edt.org --net 2hisx -p 88:88 -p 749:749 -p 464:464 -d edtasixm11/k22:kserver

ldap

a191466js@g22:~$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -d edtasixm06/ldap22:latest

----------------------------------------------------------------------------------------------------------------------

kpamldap -- khost

docker run --rm --name khost.edt.org -h khost.edt.org --net 2hisx -it edtasixm11/k22:khost
Ok install

kinit pere --> kpere

klist --> si tiene ticket

kdestroy 

cat install.sh 
#! /bin/bash

useradd unix01

apt-get update

apt-get install libpam-krb5 libpam-mount libnss-ldapd libpam-ldapd nslcd nslcd-utils ldap-utils libpam-pwquality

    Do you want to continue? [Y/n] Y

    LDAP server URI: ldap://ldap.edt.org       

    LDAP server search base: dc=edt,dc=org

    Name services to configure: 1 2

vim /etc/ldap/ldap.conf

*Dockerfile  install.sh  krb5.conf   startup.sh  

vim /etc/ldap/ldap.conf 

nmap ldap.edt.org --> 389

ldapsearch -x

service nscd start

service nslcd start

getent passwd

su - unix01

    su: warning: cannot change directory to /home/unix01: No such file or directory
    $ su - pere
    Password: 
    su: warning: cannot change directory to /tmp/home/pere: No such file or directory
    $ id
    uid=5001(pere) gid=601(professors) groups=601(professors)
    $ klist	
    Ticket cache: FILE:/tmp/krb5cc_5001_dLP4J5
    Default principal: pere@EDT.ORG

    Valid starting     Expires            Service principal
    03/27/23 08:19:39  03/27/23 18:19:39  krbtgt/EDT.ORG@EDT.ORG
        renew until 03/28/23 08:19:39
    $ exit	

$ id \
    uid=1000(unix01) gid=1000(unix01) groups=1000(unix01)

unix01@khost:/opt/docker$ su - pere --> pere

$ id \
    uid=5001(pere) gid=601(professors) groups=601(professors)

$ klist \
    klist: No credentials cache found (filename: /tmp/krb5cc_5001) \
                                                                    * No tenemos ticket pq nos hemos acreditado como usuario ldap, no con kerberos

$ exit

$ id \
    uid=1000(unix01) gid=1000(unix01) groups=1000(unix01)

$ exit

root@khost:/opt/docker# 

cat /etc/pam.d/common-auth

    auth	[success=3 default=ignore]	pam_krb5.so minimum_uid=1000
    auth	[success=2 default=ignore]	pam_unix.so nullok try_first_pass
    auth	[success=1 default=ignore]	pam_ldap.so minimum_uid=1000 use_first_pass

*pam-auth-update --> añadir o quitar perfiles /usr/share/pam-congis

tree /usr/share/pam-configs/ --> perfiles

    /usr/share/pam-configs/
    |-- capability
    |-- krb5
    |-- ldap
    |-- libpam-mount
    |-- mkhomedir
    |-- pwquality
    `-- unix

apt-get install dialog --> previo a usar pam-auth-update se debe instalar dialog

pam-auth-update --enable unix pwquality krb5 --remove ldap

vim /etc/pam.d/common-auth --> Vemos que ha cambiado la configuracio y ahora solo se puede autenticar usuario unix y kerberos, como ldap no

su - unix01

    $ su - pere --> pere
    
    $ su - pere --> kpere

    $ id
        uid=5001(pere) gid=601(professors) groups=601(professors) --> vemos que nos hemos autenticado con la passwd de ldap 'pere'

    $ klist --> tenemos ticket pues estamos autenticados por kerberos

    $ exit

$ exit

root@khost

pam-auth-update --enable unix pwquality krb5 mkhomedir --> ahora nos podran crear los homedir

su - unix01 \
    Creating directory '/home/unix01'.

su - pere --> kpere \
    Creating directory '/tmp/home/pere'.

klist

id
    uid=1000(unix01) gid=1000(unix01) groups=1000(unix01)

$ su - anna --> kanna \
    Creating directory '/tmp/home/anna'.

$ pwd \
    /tmp/home/anna
