# Kerberos
Mecanismo actual de seguridad --> LDAP + Kerberos (=FrreIPA) \
Desarrollado por MTI (Masachuset...) \
GSSAPI = Kerberos

*auth --> autenticar
autz --> autorizar
## Servicios kerberizados
    +-----------------------------------------------------------+
    |                       kserver                             |
    |                   +------------+                          |
    |   Host            |            |                          |
    | +------+          +-|-/|\-----|+                          |
    | +--|---+  <---------+  |      |                           |
    |    |      ticket       |      |   servicios kerbirizados  |
    |    |                   | SSH  |                           |
    |    |              +----|-----\|/-+                        |
    |    +------------> |              |                        |
    |                   +--------------+                        |
    +-----------------------------------------------------------+

AP --> Authentication Providers \
IP --> Information Providers

Unix  /etc/paswwd : AP + IP
LDAP  : AP + IP o solo IP
Kerberos : AP --> necessita de /etc/passwd o LDAP como IP

## Creando servidor kerberos
En /k22:kserver \
touch startup.sh install.sh

vim Dockerfile --> git

docker build -t juan22/k22:kserver .

docker run --rm --name kserver.edt.org -h kserver.edt.org --net 2hisx -p 88:88 -p 749:749 -p 464:464 -it juan22/k22:kserver /bin/bash

## Dentro del kserver
locate krb5
    /etc/krb5.conf
    /etc/krb5kdc/kdc.conf

vim /etc/krb5.conf --> git \
vim /etc/krb5kdc/kdc.conf --> git

kdb5 create -s
    key: masterkey
       : masterkey

tree /var/lib/krb5kc/ --> principals

### Crear principals
kadmin.local -q "addprinc pere" --> añadimos un principal pere -- password --> kpere \
kadmin.local -q "addprinc anna" --> kanna \
kadmin.local -q "addprinc marta" --> kmarta \
kadmin.local -q "addprinc admin/admin" --> kadmin \
kadmin.local -q "addprinc pere/admin" --> kpere \

*kadmin --> herramienta de administrador (..)

kadmin.local -q "listprincs"

kadmin.local

    kadmin.local: listprincs
    kadmin.local: addprinc ramon
    kadmin.local: delprinc ramon
    kadmin.local: ?
    kadmin.local: q

vim /etc/krb5kdc/kadm5.acl

    */admin@EDT.ORG *
    superxaxi@EDT.ORG *
    marta@EDT.ORG *

service krb5-admin-server start \
service krb5-kdc start

nmap localhost

kinit pere

klist

kdestroy --> elimina el ticket

kinit marta

kinit -p pere/admin --> -p principal (no es necesario) \
    *Destruye el ticket anterior y ahora tengo el ticket de pere/admin

kadmin

    pere/admin passwd: kpere
        :listprincs
        :add ramon --> kramon
        :listprincs
        :q
    
    destroy

kinit pere

kadmin pere

## Creando khost

docker run --rm --name khost.edt.org -h khost.edt.org --net 2hisx -it juan22/k22:khost /bin/bash

vim /etc/krb5-conf --> Git

kinit pere --> kpere
